import './plugins/import-jquery';
import 'focus-visible';
import lazyImages from './plugins/lazyImages';
import documentReady from './helpers/documentReady';
import collapce from './components/collapse';
import tabs from './components/tabs';
import accordion from './components/accordion';
import maskPhone from './plugins/phone-mask';
import Choices from 'choices.js';
import textInputBoxPlugin from './plugins/text-input-box';
import '@fancyapps/fancybox'
import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';
Swiper.use([Navigation, Pagination, Autoplay, Thumbs]);
import {WebpMachine} from "webp-hero"



documentReady(() => {
    lazyImages();
    tabs();
    collapce();
    accordion();
    maskPhone('input[name="phone"]');

    const webpMachine = new WebpMachine()
    webpMachine.polyfillDocument()

    const $body = $('body');
    textInputBoxPlugin($);
    $body.textInputBox();

    $('.btn-modal').fancybox({
        autoFocus: false,
    });


    const selectElem = document.querySelector('.pretty-select');
    if (selectElem) {
        const prettySelect = new Choices(selectElem);
    }

    // Limit Content Height
    const limitHeightOpen = document.querySelectorAll('.limit-height__button');
    if (limitHeightOpen) {
        limitHeightOpen.forEach(el => {
            el.addEventListener('click', (e) => {
                const self = e.currentTarget.closest('.limit-height');
                self.classList.toggle('open');
                return false;
            });
        });
    }

    const progress = new Swiper(".progress-slider", {
        slidesPerView: "auto",
        spaceBetween: 24,
        simulateTouch: false,
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 80,
                simulateTouch: false
            },
            1200: {
                slidesPerView: 3,
                spaceBetween: 40,
                simulateTouch: false
            },
            1400: {
                slidesPerView: 3,
                spaceBetween: 40,
                simulateTouch: false
            },
            1800: {
                slidesPerView: 3,
                spaceBetween: 188,
                simulateTouch: false
            },
        },
    });

    const serviceSlider = new Swiper(".services-slider", {
        slidesPerView: "auto",
        spaceBetween: 16,
        breakpoints: {
            768: {
                slidesPerView: "auto",
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: "auto",
                spaceBetween: 40,
            },
            1400: {
                slidesPerView: "auto",
                spaceBetween: 30,
            },
        },
    });

    // Command block
    let commandSlider = new Swiper(".command-slider", {
        loop: false,
        slidesPerView: "auto",
        spaceBetween: 16,
        breakpoints: {
            576: {
                spaceBetween: 40,
            },
            768: {
                spaceBetween: 40,
            },
            1200: {
                spaceBetween: 40,
            },
            1400: {
                spaceBetween: 40,
            },
        },
    });

    // offers item
    const offersToggle = document.querySelectorAll('.offers-item__toggle');
    if (offersToggle) {
        offersToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                const self = e.currentTarget.closest('.offers-item');
                self.classList.toggle('map-open');
                return false;
            });
        });
    }

    // Comments

    const comments = new Swiper(".comments-slider", {
        slidesPerView: "auto",
        spaceBetween: 40,
    });

    // Limit Content Height
    const commentsToggle = document.querySelectorAll('.comments__toggle');
    if (commentsToggle) {
        commentsToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                e.currentTarget.classList.toggle('open');
                document.querySelector('.comments__list').classList.toggle('open');
                return false;
            });
        });
    }

    // Partners
    const partners = new Swiper(".partners-slider", {
        slidesPerView: "auto",
        spaceBetween: 16,
        freeMode: true,
    });

    // Objects
    let projectGallery = () => {

        let gallery = new Swiper('.active .object-gallery-slider', {
            loop: false,
            navigation: {
                nextEl: '.object-gallery__nav--next',
                prevEl: '.object-gallery__nav--prev',
            },
        });

        const objects = document.querySelector('.objects');
        const objectsBtn = document.querySelectorAll('.objects__nav--button');
        const objectsContent = document.querySelectorAll('.objects__main--item');

        const galleryFilterToggle = document.querySelectorAll('.objects-filter__header');
        const galleryFilterItem = document.querySelectorAll('.objects-filter__button');

        if (objects) {
            objects.addEventListener('click', (e) => {
                if (e.target.classList.contains('objects__nav--button')) {
                    const objectsPath = e.target.dataset.objectsPath;
                    objectsBtn.forEach(el => {el.classList.remove('active')});
                    document.querySelector(`[data-objects-path="${objectsPath}"]`).classList.add('active');
                    tabsHandler(objectsPath);
                    gallery.destroy();
                    gallery = new Swiper('.active .object-gallery-slider', {
                        loop: false,
                        navigation: {
                            nextEl: '.object-gallery__nav--next',
                            prevEl: '.object-gallery__nav--prev',
                        },
                    });
                }
            });
        }

        const tabsHandler = (path) => {
            objectsContent.forEach(el => {el.classList.remove('active')});
            document.querySelector(`[data-objects-target="${path}"]`).classList.add('active');
        };

        galleryFilterToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                let self = e.currentTarget.closest('.objects-filter');
                self.classList.toggle('open');
                return false;
            });
        });

        $('.objects-filter__button').on('click', function(e){
            e.preventDefault();
            let box = $(this).closest('.objects-filter');
            let boxText = $(this).text();
            box.removeClass('open');
            box.find('.objects-filter__button').removeClass('active');
            $(this).addClass('active');
            box.find('.objects-filter__header--active').text(boxText);

            let filter = $(this).attr('data-tab');

            if (filter == 'all') {
                $("[data-filter]").removeClass("non-swiper-slide").addClass("swiper-slide").show();
                gallery.destroy();
                gallery = new Swiper('.active .object-gallery-slider', {
                    loop: false,
                    navigation: {
                        nextEl: '.object-gallery__nav--next',
                        prevEl: '.object-gallery__nav--prev',
                    },
                });
            }
            else {
                $(".active .object-gallery-slider .swiper-slide").not("[data-filter='"+filter+"']").addClass("non-swiper-slide").removeClass("swiper-slide").hide();
                $("[data-filter='"+filter+"']").removeClass("non-swiper-slide").addClass("swiper-slide").attr("style", null).show();
                gallery.destroy();
                gallery = new Swiper('.active .object-gallery-slider', {
                    loop: false,
                    navigation: {
                        nextEl: '.object-gallery__nav--next',
                        prevEl: '.object-gallery__nav--prev',
                    },
                });
            }

        });
    };
    projectGallery();

    $('.header__development--close').on('click', function(e){
        e.preventDefault();
        $(this).closest('.header__development').hide();
    });

    $('.top__button').on('click', function(e){
        e.preventDefault();
        $(this).closest('.top').hide();
    });


    // phone fix

    let phoneFix = () => {
        let header = $('.root');
        $(window).scroll(function () {
            if ($(window).scrollTop() > 0) {
                header.addClass('fixed-desktop');
            } else {
                header.removeClass('fixed-desktop');
            }
        });
    }
    phoneFix();


    let headerFix = () => {
        let header = $('.root');
        $(window).scroll(function () {
            if ($(window).scrollTop() > 0) {
                header.addClass('fix-top');
            } else {
                header.removeClass('fix-top');
            }
        });
    }
    headerFix();

    // Slider visible
    let isGalleryVisible = () => {
        let $blockOne = $('.objects__main').offset().top;
        let $blockTwo = $('.advantages').offset().top;
        let $winHeight = $(window).height();

        let startHideSidebar = $blockOne - 250;
        let stopHideSidebar = $blockTwo - 100;


        console.log($winHeight);

        $(window).scroll(function () {
            if ($(window).scrollTop() > startHideSidebar) {
                $('.root').addClass('hide-sidebar');
            } else {
                $('.root').removeClass('hide-sidebar');
            }

            if ($(window).scrollTop() > stopHideSidebar) {
                $('.root').removeClass('hide-sidebar');
            }

            if ($(window).scrollTop() + $winHeight >=  $blockOne) {
                $('.root').addClass('hide-contact');
            } else {
                $('.root').removeClass('hide-contact');
            }

            if ($(window).scrollTop() + $winHeight >=  $blockTwo) {
                $('.root').removeClass('hide-contact');
            }
        });
    }
    isGalleryVisible();

});


